# gblog
<p align="center">
  <img width="80" src="http://cdn.fengziy.cn/gblog/logo.svg"/>
</p>
<p align="center">
  <a href="https://gitee.com/fengziy/Gblog">
    <img src="https://gitee.com/fengziy/Gblog/badge/star.svg?theme=white" alt="star"/>
    <img src="https://gitee.com/fengziy/Gblog/badge/fork.svg" alt="fork"/>
  </a>
  <a href="https://github.com/fengziye/Gblog">
      <img src="https://img.shields.io/github/stars/fengziye/Gblog.svg?style=social" alt="Github star"/>
      <img src="https://img.shields.io/github/forks/fengziye/Gblog.svg?style=social" alt="Github forks"/>
  </a>
  <a href="https://github.com/vuejs/vue">
    <img src="https://img.shields.io/badge/vue-2.6.11-brightgreen.svg" alt="vue"/>
  </a>
  <a href="https://github.com/fengziye/Gblog/blob/master/license">
    <img src="https://img.shields.io/github/license/mashape/apistatus.svg" alt="license"/>
  </a>
  <a href="https://github.com/fengziye/Gblog/releases">
      <img src="https://img.shields.io/github/release/fengziye/Gblog.svg" alt="GitHub release">
  </a>
</p>

仓库：[码云](https://gitee.com/fengziy/Gblog) | [github](https://github.com/fengziye/Gblog)  
[预览demo](http://static.fengziy.cn/Gblog/)、[我的博客](http://www.fengziy.cn)
### 介绍
>
> 一款nice的基于 vue 的博客模板。
> 在一些布局样式上借鉴了[Akina For Typecho](https://zhebk.cn/Web/Akina.html)的风格，也做了一些自己的改动。  
> 打算用做搭建自己的博客使用，也开源给大家希望能喜欢，欢迎star。

### 安装依赖
```
npm install
```

### 启动服务
```
npm run serve
```

