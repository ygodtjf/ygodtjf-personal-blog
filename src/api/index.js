import request from '@/utils/request'

export function fetchList(params) {      // 获取文章列表
    return request.get('/articleList', { params: params})
}

export function fetchUserList(id) {      // 获取用户文章列表
    return request.get('/articleUserList/'+id)
}

export function fetchFocus() {      // 获取聚焦
    return request.get('/focus')
}

export function fetchCategory() {     // 获取分类
    return request.get('/category') 
}

export function fetchArticle(id) {     // 获取文章
    return request.get('/article/'+id)
}

export function fetchPostArticle(data) {     // 发布文章
    return request.post('/article', data)
}

export function fetchUpdateArticle(data) {     // 更新文章
    return request.put('/article', data)
}

export function fetchDeleteArticle(id) {     // 删除文章
    return request.delete('/article/'+id)
}

export function fetchGetImgs(data) {     // 获取图片
    return request.post('/getImgs', data)
}

export function fetchComment(id) {     // 获取评论
    return request.get('/comment/'+id)
}

export function fetchPostComment(data) {     // 发布评论
    return request.post('/comment', data)
}

export function fetchSignUp(data) {     // 注册
    return request.post('/signUp', data)
}

export function fetchSignIn(data) {     // 登录
    return request.post('/signIn', data)
}

export function fetchApplyList() {     // 获取申请列表
    return request.get('/applyList')
}

export function fetchAddApply(data){    // 申请升级
    return request.post('/apply', data)
}

export function fetchDelApply(id){    // 拒绝申请
    return request.delete('/apply/'+id)   
}

export function fetchUpdateApply(id){    // 同意申请
    return request.put('/upUserInfo', {id:id})
}

export function fetchUserInfo(email){    // 获取用户信息
    return request.get('/userInfo'+ '?email=' + email)   
}

export function fetchResetPassword(id){    // 重置密码
    return request.put('/resetPassword?id=' + id)   
}

export function fetchBlackUser(id){    // 拉黑用户
    return request.put('/blackUser?id=' + id)   
}
