import Vue from 'vue'
import App from './App.vue'
import router from './router'       // 路由
import store from './store'         // 状态管理
import './assets/css/style.less'
import './assets/font/iconfont.css'
import { parseTime } from './utils'
import hljs from 'highlight.js';        // 引入高光
import 'highlight.js/styles/atom-one-dark.css'

import mavonEditor from 'mavon-editor';  // 引入markdown
import 'mavon-editor/dist/css/index.css';

import './utils/elementui'   // 引入element-ui

import avatar from '@/const/avatar'  // 引入avatar常量

Vue.use(mavonEditor);

Vue.directive('highlight', function (el) {         // 全局注册高光指令
    let blocks = el.querySelectorAll('pre code');
    blocks.forEach((block) => {
      hljs.highlightBlock(block)
    })
})

Vue.config.productionTip = false
Vue.filter('parseTime', (v) => parseTime(v, '{y}-{m}-{d}'))
Vue.filter('getAvatar', (v) => avatar[v])
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
