import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'home',
        component: () => import('../views/Home.vue'),
        meta: { title: '首页' }
    },
    {
        path: '/category/:cate',
        name: 'category',
        component: () => import('../views/Home.vue'),
        meta: { title: '分类', params: 'cate' }
    },
    {
        path: '/search/:words',
        name: 'search',
        component: () => import('../views/Home.vue'),
        meta: { title: '搜索', params: 'words' }
    },
    // {
    //     path: '/about',
    //     name: 'about',
    //     component: () => import('../views/About.vue'),
    //     meta: { title: '关于'}
    // },
    {
        path: '/mine',
        name: 'mine',
        component: () => import('../views/Mine.vue'),
        meta: { title: '个人中心'}
    },
    {
        path: '/article/:id',
        name: 'article',
        component: () => import('../views/Articles.vue'),
        meta: { title: '文章' }
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('../views/Login.vue'),
        meta: { title: '登录' }
    },
    {
        path: '/write',
        name: 'write',
        component: () => import('../views/Write.vue'),
        meta: { title: '创作' }
    },
    {
        path:'/permission',
        name:'permission',
        component:()=>import('../views/Permission.vue'),
        meta:{title:'管理'}
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

router.beforeEach((to, from, next) => {
    let title = 'YGodBlog'
    if (to.meta.params) {
        title = `${title} - ${to.meta.title}:${to.params[to.meta.params] || ''}`
    } else {
        title = `${title} - ${to.meta.title}`
    }
    document.title = title
    if (to.path !== from.path) {
        store.dispatch('setLoading', true);
    }
    next();
})
router.afterEach((to, from) => {
    // 最多延迟 关闭 loading
    setTimeout(() => {
        store.dispatch('setLoading', false);
    }, 1500)
})
export default router
