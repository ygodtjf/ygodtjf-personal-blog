import Vue from 'vue'
import Vuex from 'vuex'
import {getTimeInterval} from '../utils/index'
// import {fetchSocial} from '@/api'

Vue.use(Vuex)
// 略:后台获取系统运行时间
const runAt = '1699885800000';
let timer = null;
const state = {
    loading: false,        // 加载
    runTimeInterval: '',
    socials: '',
    userInfo: JSON.parse(localStorage.getItem('ygod-userInfo')) || {},
}
const mutations = {
    SET_LOADING: (state, v) => {    // 加载动画
        state.loading = v;
    },
    SET_SOCIALS: (state, v) => {
        state.socials = v;
    },
    GET_RUNTIME_INTERVAL: (state) => {
        if (!timer || !state.runTimeInterval) {
            clearInterval(timer)
            timer = setInterval(() => {
                state.runTimeInterval = getTimeInterval(runAt);
            }, 1000);
        }
    },
    SER_USER_INFO: (state, v) => {
        state.userInfo = v;
    }
}
const actions = {
    setLoading: ({commit}, v) => {    // 加载动画
        commit('SET_LOADING', v);
    },
    initComputeTime: ({commit}) => {
        commit('GET_RUNTIME_INTERVAL');
    },
    // getSocials: ({commit,state}) =>{
    //     return new Promise((resolve => {
    //         if (state.socials){
    //             resolve(state.socials)
    //         } else {
    //             fetchSocial().then(res =>{
    //                 let data = res.data || []
    //                 commit('SET_SOCIALS',data);
    //                 resolve(data);
    //             }).catch(err =>{
    //                 resolve([]);
    //             })
    //         }
    //     }))
    // },
    setUserInfo: ({commit}, v) => {
        commit('SER_USER_INFO', v);
    }
}
const getters = {
    loading: state => state.loading,
    runTimeInterval: state => state.runTimeInterval,
}
export default new Vuex.Store({
    state,
    mutations,
    actions,
    modules: {},
    getters
})
