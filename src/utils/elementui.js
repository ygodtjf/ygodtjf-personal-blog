import Vue from 'vue'

import {
  Button,
  Switch,
  Input,
  Select,
  Form,
  FormItem,
  Upload,
  Row,
  Col,
  Option,
  Message
}  from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';



Vue.use(Button);
Vue.use(Switch);
Vue.use(Input);
Vue.use(Select);
Vue.use(Form);
Vue.use(FormItem);
Vue.use(Upload);
Vue.use(Row);
Vue.use(Col);
Vue.use(Option);

Vue.prototype.$message = Message;